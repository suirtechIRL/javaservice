#!/usr/bin/env bash

clear
# build st-test-env
echo starting building...

mvn clean package -DskipTests

docker build -t suirtech/javaservice:latest .