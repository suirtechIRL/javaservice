FROM java:8
MAINTAINER shane@suirtech.com
VOLUME /tmp
EXPOSE 8080
ENV USER_NAME javauser
ENV APP_NAME javaservice
ENV APP_HOME /home/$USER_NAME/$APP_NAME
RUN useradd -ms /bin/bash $USER_NAME
RUN mkdir $APP_HOME
ADD target/java-service-1.0-SNAPSHOT.jar $APP_HOME/$APP_NAME.jar
RUN chown $USER_NAME $APP_HOME/$APP_NAME.jar
USER $USER_NAME
WORKDIR $APP_HOME
RUN bash -c 'touch javaservice.jar'
ENTRYPOINT ["java","-jar","javaservice.jar"]
