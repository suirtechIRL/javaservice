package com.suirtech.auto.demo.javaservice.controllers;

import com.suirtech.auto.demo.javaservice.model.Health;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {

    private static Logger logger = LoggerFactory.getLogger(HealthController.class);

    @GetMapping("/health")
    public Health health() {
        Health h =  new Health();
        logger.info(h.toString());
        return h;
    }

}
