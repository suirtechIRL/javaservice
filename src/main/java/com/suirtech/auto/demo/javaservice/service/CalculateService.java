package com.suirtech.auto.demo.javaservice.service;

import org.springframework.stereotype.Service;

@Service
public class CalculateService {

    public int add(int a, int b) {
        return a + b;
    }

    public int subtract(int b, int a) {
        return b - a;
    }
}
