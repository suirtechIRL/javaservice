package com.suirtech.auto.demo.javaservice.model;

import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Calendar;

@Data
public class Health {

    private String status;
    private String message;
    private String date;

    public Health() {
        this.date =  new SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
                .format(Calendar.getInstance()
                        .getTime());
        this.status = "200";
        this.message = "All Good";
    }
}
