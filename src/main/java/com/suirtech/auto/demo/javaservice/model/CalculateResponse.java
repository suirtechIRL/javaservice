package com.suirtech.auto.demo.javaservice.model;

import lombok.Data;

@Data
public class CalculateResponse {
    int param1;
    int param2;
    int result;
    String function;


    public CalculateResponse(int a, int b, int result, String function){
        this.result = result;
        this.function = function;
        this.param1 = a;
        this.param2 = b;
    }
}
