package com.suirtech.auto.demo.javaservice.controllers;

import com.suirtech.auto.demo.javaservice.model.MessageResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

@RestController
public class MQController {

    private static Logger logger = LoggerFactory.getLogger(MQController.class);

    @Autowired
    private JmsTemplate jmsTemplate;

    @GetMapping("/send")
    MessageResponse send(@RequestParam(value = "message", defaultValue = "1") String message) {
        logger.info("SENDING MESSAGE : " + message );
        jmsTemplate.send("DEV.QUEUE.1", new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                return session.createTextMessage(message);
            }
        });
        return new MessageResponse("sent : " + message);
    }

    @GetMapping("/recv")
    MessageResponse recv(){
        logger.info("REC MESSAGE");
        String result = "no message";
        Message message = jmsTemplate.receive("DEV.QUEUE.1");
        TextMessage textMessage = (TextMessage) message;
        try {
            String text = textMessage.getText();
            logger.info("received: " + text);
        } catch (JMSException e) {
            e.printStackTrace();
        }
        try {
            result = textMessage.getText();
        } catch (JMSException e) {
            e.printStackTrace();
        }
        return new MessageResponse(result);
    }

}
