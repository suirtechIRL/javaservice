package com.suirtech.auto.demo.javaservice.controllers;

import com.suirtech.auto.demo.javaservice.model.CalculateResponse;
import com.suirtech.auto.demo.javaservice.service.CalculateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculateController {

    private static Logger logger = LoggerFactory.getLogger(CalculateController.class);

    @Autowired
    private CalculateService calculateService;

    @GetMapping("/add")
    public CalculateResponse add(@RequestParam(value = "a", defaultValue = "1") int a,
                                 @RequestParam(value = "b", defaultValue = "1") int b) {
        logger.info("/add " + a + " : " + b);
        return new CalculateResponse(a, b,
                calculateService.add(b,a),
                "ADDITION");
    }

    @GetMapping("/subtract")
    public CalculateResponse subtract(@RequestParam(value = "a", defaultValue = "1") int a,
                        @RequestParam(value = "b", defaultValue = "1") int b) {
        logger.info("/subtract " + a + " from " + b);
        return new CalculateResponse(a, b,
                calculateService.subtract(b,a),
                "SUBTRACTION");
    }

}
