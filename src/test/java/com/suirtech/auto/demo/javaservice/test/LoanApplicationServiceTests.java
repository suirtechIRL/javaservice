package com.suirtech.auto.demo.javaservice.test;

import com.suirtech.auto.demo.javaservice.model.MessageResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureStubRunner(ids = {"com.suirtech.auto:java-service:+:stubs:8080"}, stubsMode = StubRunnerProperties.StubsMode.LOCAL)
public class LoanApplicationServiceTests {

    private RestTemplate restTemplate = new RestTemplate();


    @Test
    public void testExample() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);


        ResponseEntity<MessageResponse> response = restTemplate.exchange("http://localhost:8080/send", HttpMethod.GET, entity, MessageResponse.class);
        System.out.println(response.getBody().getMessage());
    }

}