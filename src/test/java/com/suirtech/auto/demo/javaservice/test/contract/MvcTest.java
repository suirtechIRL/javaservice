package com.suirtech.auto.demo.javaservice.test.contract;

import com.suirtech.auto.demo.javaservice.controllers.MQController;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;

public class MvcTest {

    @Before
    public void setup() {
        RestAssuredMockMvc.standaloneSetup(new MQController());
    }

}