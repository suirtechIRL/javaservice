package com.suirtech.auto.demo.javaservice.test.unit;

import com.suirtech.auto.demo.javaservice.service.CalculateService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class TestCalculateService {


    @Autowired
    private CalculateService service;


    @Test
    @DisplayName("Unit: add()")
    void testAdd() {
        int a = 5;
        int b = 7;
        int expected = 12;
        int actual = service.add(a, b);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void testAddFail() {
        int a = 5;
        int b = 12;
        int expected = 12;
        int actual = service.add(a, b);

        Assertions.assertNotEquals(expected, actual);
    }

    @Test
    void testSubtraction() {
        int a = 5;
        int b = 12;
        int expected = 7;
        int actual = service.subtract(b, a);

        Assertions.assertEquals(expected, actual);
    }


    @Test
    void testSubtractionFail() {
        int a = 5;
        int b = 12;
        int expected = 16;
        int actual = service.subtract(b, a);

        Assertions.assertNotEquals(expected, actual);
    }

}
