package com.suirtech.auto.demo.javaservice.test.integration;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class Int1 {

    private WireMockServer wireMockRule = new WireMockRule(WireMockConfiguration.options().port(8888).httpsPort(8889));


    @Test
    public void bothServicesDoStuff() {
        wireMockRule.start();
        configureFor("localhost", 8888);
        getSend(wireMockRule);

        RestTemplate restTemplate = new RestTemplate();
        String rep = restTemplate.getForObject("http://localhost:8888/send", String.class);
        Assert.assertEquals(true, rep.contains("Hello from wiremock"));
        wireMockRule.stop();
    }

    public void getSend(WireMockServer mockInstance) {
        mockInstance.stubFor(get(urlPathMatching("/send.*"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("\"message\": \"Hello from wiremock\"")));

    }
}
