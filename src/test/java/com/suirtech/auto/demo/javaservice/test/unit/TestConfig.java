package com.suirtech.auto.demo.javaservice.test.unit;

import com.suirtech.auto.demo.javaservice.service.CalculateService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration

@ComponentScan({
        "com.suirtech.auto.demo.javaservice"
})

public class TestConfig {

}
