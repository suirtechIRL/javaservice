package com.suirtech.auto.demo.javaservice.test.mockservice;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.suirtech.auto.demo.javaservice.test.integration.Int1;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class MockRunner {

    public static void main(String[] args){
        Int1 int1 = new Int1();


        System.out.println("java service mocker ");
        WireMockServer wireMockServer = new WireMockServer(options().port(7777));
        wireMockServer.start();

        int1.getSend(wireMockServer);

    }
}
