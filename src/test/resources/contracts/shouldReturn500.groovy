package contracts

org.springframework.cloud.contract.spec.Contract.make {
    request {
        method 'GET'
        url '/500'
        headers {
            header('Content-Type', 'application/json')
        }
    }
    response {
        status 500
        headers {
            header('Content-Type': 'application/json')
        }
    }
}