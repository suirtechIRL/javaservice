package contracts

org.springframework.cloud.contract.spec.Contract.make {
    request {
        method 'GET'
        url '/503'
        headers {
            header('Content-Type', 'application/json')
        }
    }
    response {
        status 503
        headers {
            header('Content-Type': 'application/json')
        }
    }
}