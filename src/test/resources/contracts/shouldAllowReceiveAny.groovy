package contracts

org.springframework.cloud.contract.spec.Contract.make {
    request {
        method 'GET'
        url '/recv'
        headers {
            header('Content-Type', 'application/json')
        }
    }
    response {
        status 200
        body("""
  {
    "message": "Recv, you have been mocked, !"
  }
  """)
        headers {
            header('Content-Type': 'application/json')
        }
    }
}