package contracts


org.springframework.cloud.contract.spec.Contract.make {
    request {
        method 'GET'
        url'/send?message=hi'

    }
    response {
        status 200
        body("""
  {
    "message": "you have been mocked, but all ok !"
  }
  """)
        headers {
            header('Content-Type': 'application/json')
        }
    }
}
